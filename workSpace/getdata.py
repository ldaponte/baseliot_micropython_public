import utime
import json
import urequests
import config

thing = config.Thing()

CITY = "Basel"

def get_weather():
    r = urequests.get("http://api.openweathermap.org/data/2.5/weather?q=%s&appid=%s" % (CITY, thing.OpenWeatherMapKey)).json()
    return r
