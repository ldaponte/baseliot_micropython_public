# This file is executed on every boot (including wake-boot from deepsleep)
#import esp
#esp.osdebug(None)
import gc
import webrepl
import network
import config
import main

thing = config.Thing()

webrepl.start()
gc.collect()

# Setup network interfaces
sta = network.WLAN(network.STA_IF)
ap = network.WLAN(network.AP_IF)

# Setup SSID of access point
ap.config(essid=thing.hostname)

sta.active(True)
sta.disconnect()

# Make sure ESP gives a custom host name when asking DHCP for IP address
sta.config(dhcp_hostname=thing.hostname)

# Get WiFi credentials from configuration
sta.connect(thing.WiFISSID, thing.WiFiPassword)

# Run our main application
main.run()