import ssd1306
import framebuf
import machine
import urequests
import utime
import config

i2c = machine.I2C(sda=machine.Pin(4), scl=machine.Pin(5))
display = ssd1306.SSD1306_I2C(64, 48, i2c)
thing = config.Thing()

# CITY = "Basel"
CITY = "Phoenix"

ICONS = {
    '00': (
        0b00000000,
        0b01000010,
        0b00100100,
        0b00011000,
        0b00011000,
        0b00100100,
        0b01000010,
        0b00000000,
    ),
    '01': (
        0b00010000,
        0b01010010,
        0b00111100,
        0b00111111,
        0b11111100,
        0b00111100,
        0b01001010,
        0b00001000,
    ),
    '02': (
        0b00010000,
        0b01010100,
        0b00111000,
        0b11111110,
        0b01000100,
        0b10000010,
        0b10000001,
        0b01111110,
    ),
    '03': (
        0b00000000,
        0b00000000,
        0b00000000,
        0b00111000,
        0b01000100,
        0b10000010,
        0b10000001,
        0b01111110,
    ),
    '04': (
        0b01100000,
        0b10011000,
        0b10000100,
        0b01111000,
        0b01000100,
        0b10000010,
        0b10000001,
        0b01111110,
    ),
    '09': (
        0b00111100,
        0b01111110,
        0b11111111,
        0b11111111,
        0b10001001,
        0b00001000,
        0b00101000,
        0b00111000,
    ),
    '10': (
        0b00101010,
        0b00011100,
        0b01111111,
        0b00011100,
        0b01111010,
        0b11111100,
        0b00010000,
        0b00110000,
    ),
    '11': (
        0b00001111,
        0b00011110,
        0b00111100,
        0b01111111,
        0b11111110,
        0b00000100,
        0b00001000,
        0b00010000,
    ),
    '13': (
        0b01010010,
        0b11010011,
        0b00111100,
        0b00100111,
        0b11100100,
        0b00111100,
        0b11001011,
        0b01001010,
    ),
    '50': (
        0b11111111,
        0b00000000,
        0b11111111,
        0b00000000,
        0b11111111,
        0b00000000,
        0b11111111,
        0b00000000,
    ),
}
DIGITS = {
    ' ': (
        0b0000,
        0b0000,
        0b0000,
        0b0000,
        0b0000,
    ),
    '0': (
        0b0111,
        0b0101,
        0b0101,
        0b0101,
        0b0111,
    ),
    '1': (
        0b0010,
        0b0010,
        0b0010,
        0b0010,
        0b0010,
    ),
    '2': (
        0b0111,
        0b0001,
        0b0111,
        0b0100,
        0b0111,
    ),
    '3': (
        0b0111,
        0b0001,
        0b0111,
        0b0001,
        0b0111,
    ),
    '4': (
        0b0101,
        0b0101,
        0b0111,
        0b0001,
        0b0001,
    ),
    '5': (
        0b0111,
        0b0100,
        0b0111,
        0b0001,
        0b0111,
    ),
    '6': (
        0b0111,
        0b0100,
        0b0111,
        0b0101,
        0b0111,
    ),
    '7': (
        0b0111,
        0b0001,
        0b0001,
        0b0001,
        0b0001,
    ),
    '8': (
        0b0111,
        0b0101,
        0b0111,
        0b0101,
        0b0111,
    ),
    '9': (
        0b0111,
        0b0101,
        0b0111,
        0b0001,
        0b0001,
    ),
    ':': (
        0b0000,
        0b0001,
        0b0000,
        0b0001,
        0b0000,
    ),
    '.': (
        0b0000,
        0b0000,
        0b0000,
        0b0000,
        0b0001,
    ),
    '-': (
        0b0000,
        0b0000,
        0b0001,
        0b0000,
        0b0000,
    ),
}

wait = 0


def show(image, dx=0, dy=0):
    for y, row in enumerate(image):
        bit = 1
        for x in range(8):
            display.pixel(dx + x, dy + y, row & bit)
            bit <<= 1
    display.show()


def show_text(text):
    x = 0
    for c in reversed(text):
        if c in '.:-':
            show(DIGITS.get(c, DIGITS[' ']), x - 1, 1)
        else:
            show(DIGITS.get(c, DIGITS[' ']), x, 1)
            x += 4


def get_weather():
    r = urequests.get("http://api.openweathermap.org/data/2.5/weather"
                      "?q=%s&appid=%s" % (CITY, thing.OpenWeatherMapKey)).json()
    return r["weather"][0]["icon"], int(r["main"]["temp"] - 273.15)

while True:
    if wait <= 0:
        icon, temp = get_weather()
        wait = 60

    display.fill(0)
    show_text('%2d' % temp)
    utime.sleep(4)
    show(ICONS[icon[:2]])
    utime.sleep(4)
    wait -= 1
