import uasyncio as asyncio

async def bar1():
    count = 0
    while True:
        count += 1
        print('bar1: {}'.format(count))
        await asyncio.sleep(1)  # Pause 1s

async def bar2():
    count = 0
    while True:
        count += 1
        print('bar2: {}'.format(count))
        await asyncio.sleep(2)  # Pause 1s

async def killer():
    await asyncio.sleep(10)

loop = asyncio.get_event_loop()
loop.create_task(bar1()) # Schedule ASAP
loop.create_task(bar2()) # Schedule ASAP
loop.run_until_complete(killer())