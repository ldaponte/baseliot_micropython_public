import time
import lcdgfx as disp
import math

# Draw an analog clock face
def drawFace(lcd, screenW, screenH):

    clockCenterX = int(screenW / 2)
    clockCenterY = int(screenH / 2)
    clockRadius = 16
    clockRadiusOuter = 21

    x = 1
    y = 1

    lcd.fill(0)
    disp.drawCircle(clockCenterX + x, clockCenterY + y, clockRadiusOuter, lcd, 1)

    # hour ticks
    for z in range(360)[0::30]:
        # Begin at 0° and stop at 360°
        angle = float(z)
        angle = ( angle / 57.29577951 ) # Convert degrees to radians
        x2 = int( clockCenterX + ( math.sin(angle) * clockRadius ) )
        y2 = int( clockCenterY - ( math.cos(angle) * clockRadius ) )
        x3 = int( clockCenterX + ( math.sin(angle) * ( clockRadius - ( clockRadius / 8 ) ) ) )
        y3 = int( clockCenterY - ( math.cos(angle) * ( clockRadius - ( clockRadius / 8 ) ) ) )
        disp.drawLine( x2 + x , y2 + y , x3 + x , y3 + y, lcd, 1)

# Draw the clock's three arms: seconds, minutes, hours.
def drawArms(lcd, screenW, screenH, h, m, s):
    # disp second hand

    clockCenterX = int(screenW / 2)
    clockCenterY = int(screenH / 2)
    clockRadius = 16
    clockRadiusOuter = 21
    
    x = 1
    y = 1

    angle = float(s * 6)
    angle = ( angle / 57.29577951 ) # Convert degrees to radians
    x3 = int( clockCenterX + ( math.sin(angle) * ( clockRadius - ( clockRadius / 5 ) ) ) )
    y3 = int( clockCenterY - ( math.cos(angle) * ( clockRadius - ( clockRadius / 5 ) ) ) )
    disp.drawLine( clockCenterX + x , clockCenterY + y , x3 + x , y3 + y, lcd, 1)
    #
    # disp minute hand
    angle = m * 6
    angle = ( angle / 57.29577951 )  # Convert degrees to radians
    x3 = int( clockCenterX + ( math.sin(angle) * ( clockRadius - ( clockRadius / 4 ) ) ) )
    y3 = int( clockCenterY - ( math.cos(angle) * ( clockRadius - ( clockRadius / 4 ) ) ) )
    disp.drawLine( clockCenterX + x , clockCenterY + y , x3 + x , y3 + y, lcd, 1)
    #
    # disp hour hand
    angle = h * 30 + int( ( m / 12 ) * 6 )
    angle = ( angle / 57.29577951 )  # Convert degrees to radians
    x3 = int( clockCenterX + ( math.sin(angle) * ( clockRadius - ( clockRadius / 2 ) ) ) )
    y3 = int( clockCenterY - ( math.cos(angle) * ( clockRadius - ( clockRadius / 2 ) ) ) )
    disp.drawLine( clockCenterX + x , clockCenterY + y , x3 + x , y3 + y, lcd, 1)