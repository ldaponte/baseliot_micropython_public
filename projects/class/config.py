# Configuration for each ESP device based on their serial number
# For classroom setup
# Includes analog clock face utility code

import machine
from ubinascii import hexlify

class Thing(object):

    def __init__(self):

        self.hostname = "your unique micropython host name here"

        self.WiFISSID = 'wifi ssid'
        self.WiFiPassword = 'wifi password'

        self.MQTTClientId = hexlify(machine.unique_id())
        self.MQTTHost = 'io.adafruit.com'
        self.MQTTPort = 1883
        
        self.MQTTUser = 'aio user name'
        self.MQTTPassword = 'aio active key'

        self.MQTTTopicTemp = 'aio user name/feeds/temp feed name'
        self.MQTTTopicHumidity = 'aio user name/feeds/humidity feed name'

        self.OpenWeatherMapKey = 'your open weather map key here'