# Send temperature to Adafruit or other MQTT site
# Classroom with chart results

import time
import ssd1306
from umqtt.robust import MQTTClient
from ubinascii import hexlify
import uasyncio
import config
import dht12
from machine import I2C, Pin

thing = config.Thing()

messageCount = 0
i2c =I2C(sda=Pin(4), scl=Pin(5))
sensor = dht12.DHT12(i2c)
display = ssd1306.SSD1306_I2C(64, 48, i2c)
loop = uasyncio.get_event_loop()
pubclient = MQTTClient(thing.MQTTClientId, thing.MQTTHost, thing.MQTTPort, thing.MQTTUser, thing.MQTTPassword)
interval = 5

# Display Temperature
def displayTemp(message, temp, humidity):

    display.fill(0)

    display.text('C: ' + message, 1, 10)
    display.text('T: ' + temp, 1, 25)
    display.text('H: ' + humidity, 1, 40)
 
    display.show()
    
def pushTemp():

    global messageCount
    
    sensor.measure()
    messageCount += 1

    message = str(messageCount)
    temp = str(sensor.temperature())
    humidity = str(sensor.humidity())

    displayTemp(message, temp, humidity)

    print('C: ' + message)
    print('T: ' + temp)
    print('H: ' + humidity)
    
    pubclient.publish(thing.MQTTTopicTemp, temp, 0)
    pubclient.publish(thing.MQTTTopicHumidity, humidity, 0)
    loop.call_later(interval, pushTemp) # Schedule.

def run():

    pubclient.connect()
    loop.call_soon(pushTemp) # Schedule after 2 seconds.
    loop.run_forever()