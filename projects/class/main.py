# Default main - do nothing
import network
import machine

def run():
    print('up!')

def wifi():
    sta_if = network.WLAN(network.STA_IF)
    if sta_if.isconnected():
        print('WLAN connected!')
        print(sta_if.ifconfig())
    else:
        print('WLAN not connected')
    